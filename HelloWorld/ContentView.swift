//
//  ContentView.swift
//  HelloWorld
//
//  Created by James Hartshorn on 01/08/2022
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Goodbye, cruel world!")
            Button("Click") {
                //action
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
