//
//  HelloWorldApp.swift
//  HelloWorld
//
//  Created by James Hartshorn on 01/08/2022.
//

import SwiftUI

@main
struct HelloWorldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
